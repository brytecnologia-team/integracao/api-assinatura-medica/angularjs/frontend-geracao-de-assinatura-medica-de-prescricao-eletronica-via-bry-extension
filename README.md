### Inclusão de Metadados em Prescrição Eletrônica com Assinatura PDF via BRy Extension - Frontend

Aplicação PDF de exemplo que realiza a integração com o plugin BRy Extension para realizar as seguintes operações:

- Listar os certificados digitais instalados no sistema operacional;
- Realizar cifragem dos atributos assinados obtidos na etapa de inicialização e codificá-los em base64;
- Realizar a inclusão dos Metadados de Prescrição Eletrônica.

Foi utilizado o arquivo script-customizavel.js, disponibilizado na documentação do plugin, para realizar as chamadas das funções do BRy Extension. Esse script está localizado na pasta assets do projeto.

<b>Realiza comunicação com o [backend] intermediário o qual realizará comunicação com o serviço de assinatura do Framework</b>

O documento será assinado em conjunto com a inclusão dos metadados.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para geração de assinatura.

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.


**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Autenticação, OID's e seus respectivos valores

Para entendimento dos requisitos de Autenticação, recomendamos que leia o artigo [Assinatura PDF Etapa Única](https://api-assinatura.bry.com.br/api-assinatura-digital#assinatura-pdf-etapa-unica).

Os valores correspondentes a cada tipo de OID podem ser encontrados no endereço [API Assinatura Prescrição](https://api-assinatura.bry.com.br/api-assinatura-medica#assinatura-da-prescricao).

### Uso

Para executar a aplicação é necessário executar os seguintes passos:

1 - Certifique-se de que possui a BRy Extension instalada em seu navegador.
 
2 - Instalar as dependências utilizando o comando:

    npm install

2 - Iniciar a aplicação:

    npm run start

É preciso executar o comando npm run start, ao invés, de ng serve devido a utilização do proxy. Esse proxy é utilizado para conseguir realizar a comunicação com o serviço back-end. A configuração do proxy está definida no arquivo proxy.config.js armazenado na pasta raiz do projeto.

[backend]: <https://gitlab.com/brytecnologia-team/integracao/api-assinatura-medica/java/geracao-de-assinatura-medica-de-prescricao-eletronica-pkcs1>
