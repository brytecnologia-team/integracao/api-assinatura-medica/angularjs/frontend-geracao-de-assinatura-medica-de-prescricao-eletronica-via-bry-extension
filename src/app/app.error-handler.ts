import {ErrorHandler, Injectable, Injector, NgZone} from '@angular/core'
import {HttpErrorResponse} from '@angular/common/http'
import { d } from '@angular/core/src/render3'

//import {throwError} from 'rxjs/operators'

@Injectable()
export class ApplicationErrorHandler extends ErrorHandler{

  constructor(private injector: Injector,
              private zone: NgZone){ //
    super()
  }

  handleError(errorResponse: HttpErrorResponse | any){
    if(errorResponse instanceof HttpErrorResponse){
      const message = errorResponse.error.message
      this.zone.run(()=>{
        switch(errorResponse.status){
          case 401:
            alert('Não autorizado (token inválido)');
            break;
          case 403:
            alert(message || 'Não autorizado (token inválido).');
          case 404:
            alert(message || 'Recurso não encontrado. Verifique o console para mais detalhes');
            break;
          default:
            if(errorResponse.error){
              var erros = errorResponse.error.error;
              alert(JSON.stringify(erros));
            }else{
              alert('Erro ao executar operação solicitada');
            }
        }
      })
    }
    super.handleError(errorResponse)
  }
}
