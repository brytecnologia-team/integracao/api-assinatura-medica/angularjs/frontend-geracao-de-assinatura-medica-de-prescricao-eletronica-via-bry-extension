import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms'
import { SignatureService } from './assinatura.service'
import { tap } from 'rxjs/operators'
import { tipodocumentos } from '../model/tipodocumentos';
import { tipomedicos } from '../model/tipodocumentos';
declare var BryApiModule: any;

@Component({
  selector: 'cms-assinatura',
  templateUrl: './assinatura.component.html',
  styleUrls: ['./assinatura.component.css']
})
export class InitializeComponent implements OnInit {

  signatureForm: FormGroup;

  inicializationResult;

  finalizationResult;

  file: any;

  imageUpload: any;

  signedAttributes;

  initializedDocument;

  base64Certificate;

  title = 'PDF Signature';

  auxSignatureValue;

  signature;

  token;

  nonces;

  numeroOID;

  UFOID;

  especialidadeOID;

  json;

  metadados: any;

  tipo: any;

  documentos = [
    new tipodocumentos('2.16.76.1.12.1.1', 'Prescrição de medicamento'),
    new tipodocumentos('2.16.76.1.12.1.2', 'Atestado médico'),
    new tipodocumentos('2.16.76.1.12.1.3', 'Solicitação de exame'),
    new tipodocumentos('2.16.76.1.12.1.4', 'Laudo laboratorial'),
    new tipodocumentos('2.16.76.1.12.1.5', 'Sumária de alta'),
    new tipodocumentos('2.16.76.1.12.1.6', 'Registro de atendimento clinico'),
    new tipodocumentos('2.16.76.1.12.1.7', 'Dispensação de medicamentos'),
    new tipodocumentos('2.16.76.1.12.1.8', 'Vacinação'),
    new tipodocumentos('2.16.76.1.12.1.11', 'Relatório médico')
  ];

  tiposProfissional = [
    new tipomedicos('Médico'),
    new tipomedicos('Farmacêutico'),
    new tipomedicos('Odontologista'),
  ];

  constructor(private signatureService: SignatureService,
    private router: Router,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.signatureForm = this.formBuilder.group({
      tipoDocumento: new FormControl('2.16.76.1.12.1.1'),
      tipo: new FormControl('Médico', { validators: [Validators.required], updateOn: 'blur' }),
      numero: new FormControl('', { validators: [Validators.required], updateOn: 'blur' }),
      uf: new FormControl('', { validators: [Validators.required], updateOn: 'blur' }),
      especialidade: new FormControl(''),
      token: new FormControl('', { validators: [Validators.required], updateOn: 'blur' }),
      profile: new FormControl('BASIC', { validators: [Validators.required], updateOn: 'blur' }),
      hashAlgorithm: new FormControl('SHA256', { validators: [Validators.required], updateOn: 'blur' }),
      certificate: new FormControl(''),
      file: this.formBuilder.control(null, [Validators.required]),
      image: this.formBuilder.control(''),
      height: new FormControl(60),
      width: new FormControl(170),
      position: new FormControl('INFERIOR_ESQUERDO'),
      page: new FormControl('PRIMEIRA'),
      signature: new FormControl('')
    })
  }

  initialize(signatureForm: FormGroup, base64Certificate) {

    this.token = this.signatureForm.get('token').value;

    this.base64Certificate = base64Certificate;

    this.nonces = `PDF${this.signatureService.generateRandonNumber()}`;
    console.log('Nonces', this.nonces);


    this.getOIDEspecialidade();
    this.getOIDNumero();
    this.getOIDUF();

    var jsonEspecialidade = {
      especialidade: this.signatureForm.value.especialidade,
      valor: this.especialidadeOID
    }
    var jsonUF = {
      UF: this.signatureForm.value.uf,
      valor: this.UFOID
    }
    var jsonNumero = {
      numero: this.signatureForm.value.numero,
      valor: this.numeroOID
    }
    this.json = {
      tipo: this.signatureForm.value.tipoDocumento,
      numero: jsonNumero,
      UF: jsonUF,
      especialidade: jsonEspecialidade,
    }

    this.signatureService.initialize(this.nonces, signatureForm, this.base64Certificate, this.file, this.imageUpload, this.token, this.json)
      .pipe(
        tap((result: any) => {
          this.inicializationResult = result;
        }
        )
      ).subscribe(async (result: any) => {

        console.log(`Inicialization result sem o parse: ${JSON.stringify(result)}`);

        let nonceRequisicao = result.nonce;

        const preparedExtensionData = await this.prepareInputDataForExtension(result);

        console.log('Dados preparados da extensao:', preparedExtensionData);

        const signedData = await this.signDataByExtension(preparedExtensionData);

        console.log('Dados assinados pela extensao: ', signedData);

        var objPreparedExtensionData = JSON.parse(signedData);

        this.auxSignatureValue = objPreparedExtensionData.assinaturas[0].hashes[0];

        console.log('Encrypted signed attributes and Base64 encoded: ', this.auxSignatureValue);

        this.signatureService.finalize(nonceRequisicao, this.nonces, this.auxSignatureValue, this.token)
          .pipe(
            tap((result: any) => {
              this.finalizationResult = result;
            }
            )
          ).subscribe((result: any) => {

            console.log(`Finalization result: ${JSON.stringify(result)}`);

            this.signature = JSON.stringify(result)

            this.signatureForm.get('signature').setValue(this.signature);

            console.log('link', result.documentos[0].links[0].href);

            let signatureID = result.identificador;

            this.signatureService.getSignature(result.documentos[0].links[0].href).subscribe((result: any) => {
              let blob = new Blob([result], { type: 'application/pdf' });

              var downloadURL = window.URL.createObjectURL(blob);
              var link = document.createElement('a');
              link.href = downloadURL;
              link.download = `signature-${signatureID}.pdf`;
              link.click();
              alert('Signature realized successful!');
            });
          })
      })
  }

  clear() {
    this.tipo = null;
    this.signature = null;
    this.file = null;
    this.imageUpload = null;
    this.signatureForm.get('image').reset();
    this.signatureForm.get('image').setValue(null);
    this.signatureForm.get('file').reset()
    this.signatureForm.get('file').setValue(null);
    this.signatureForm.get('hashAlgorithm').setValue('SHA256');
  }

  getTipoProfissional() {
    return this.signatureForm.get('tipo').value;
  }

  getOIDNumero() {
    if (this.signatureForm.get('tipo').value == 'Médico') {
      this.numeroOID = '2.16.76.1.4.2.2.1';
    } else if (this.signatureForm.get('tipo').value == 'Farmacêutico') {
      this.numeroOID = '2.16.76.1.4.2.3.1';
    } else if (this.signatureForm.get('tipo').value == 'Odontologista') {
      this.numeroOID = '2.16.76.1.4.2.12.1';
    }
  }
  getOIDUF() {
    if (this.signatureForm.get('tipo').value == 'Médico') {
      this.UFOID = '2.16.76.1.4.2.2.2';
    } else if (this.signatureForm.get('tipo').value == 'Farmacêutico') {
      this.UFOID = '2.16.76.1.4.2.3.2';
    } else if (this.signatureForm.get('tipo').value == 'Odontologista') {
      this.UFOID = '2.16.76.1.4.2.12.2';
    }
  }
  getOIDEspecialidade() {
    if (this.signatureForm.get('tipo').value == 'Médico') {
      this.especialidadeOID = '2.16.76.1.4.2.2.3';
    } else if (this.signatureForm.get('tipo').value == 'Farmacêutico') {
      this.especialidadeOID = '2.16.76.1.4.2.3.3';
    } else if (this.signatureForm.get('tipo').value == 'Odontologista') {
      this.especialidadeOID = '2.16.76.1.4.2.12.3';
    }
  }

  fillMetadadosDataForm() {
    this.getOIDNumero();
    this.getOIDUF();
    this.getOIDEspecialidade();
  }

  fillCertificateDataForm() {
    BryApiModule.fillCertificateDataForm();
  }

  listCertificates() {
    BryApiModule.listCertificates();
  }

  prepareInputDataForExtension(result): any {
    const response = BryApiModule.prepareDataForExtension(result);
    return response;
  }

  async signDataByExtension(prepareInputData) {
    const signedData = BryApiModule.sign(prepareInputData);
    return signedData;
  }

  upload(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }

  uploadImage(event) {
    if (event.target.files.length > 0) {
      this.imageUpload = event.target.files[0];
    } else {
      this.imageUpload = null;
    }
  }
}
